//RECEIVER

#include <Arduino.h>

//nRF24L01 Libraries
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

//I2C Libraries
#include <Wire.h>

//
#include "mcp4728.h"


RF24 radio(7, 8); // CE, CSN
const byte address[6] = "00001";

mcp4728 dac = mcp4728(0); //ID = 0


void setup() {

	Serial.begin(9600);
	dac.begin();  // initialize i2c interface
	dac.vdd(5000); // set VDD(mV) of MCP4728

	//	int id = dac.getId(); // return devideID of object
	//	Serial.print("Device ID  = "); // serial print of value
	//	Serial.println(id, DEC); // serial print of value

	radio.begin();
	radio.openReadingPipe(0,address); // 00001
	radio.setPALevel(RF24_PA_MIN);
	radio.startListening();


}
void loop() {
	delay(5);

	unsigned int voltages[] = {0,0,0};

	if ( radio.available()) {

		radio.read(&voltages, sizeof(voltages));

		Serial.println(voltages[0]);
//		Serial.println(voltages[1]);
//		Serial.println(voltages[2]);

		dac.analogWrite(voltages[0],voltages[1],voltages[2],0);

		//
	}
}
