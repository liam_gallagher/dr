//TRANSMITTER

#include <Arduino.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); // CE, CSN

const byte address[6] = "00001";

void setup() {

	Serial.begin(9600);
	radio.begin();
	radio.openWritingPipe(address); // 00001
	radio.setPALevel(RF24_PA_MIN);
	radio.stopListening();
}
void loop() {

	unsigned int voltages[] = {analogRead(A0),analogRead(A1),analogRead(A2)};

	//	delay(5);

	//	int potValue0 = analogRead(A0);
	//	int potValue1 = analogRead(A1);
	//	int potValue2 = analogRead(A2);

	//	voltages[0] = 45.4;
	//	voltages[1] = 12.4;
	//	voltages[2] = 2.4;


//	for (int i = 0;i<5000;i++){
//
//		voltages[0] = i;
//		voltages[1] = i;
//		voltages[2] = i;

		radio.write(&voltages, sizeof(voltages));
//	}
	delay(5);

	//	int potValue0 = 500;
	//	int potValue1 = 750;
	//	int potValue2 = 1000;
	//
	//	float voltage0 = potValue0*(5.0/1023.0);
	//	float voltage1 = potValue1*(5.0/1023.0);
	//	float voltage2 = potValue2*(5.0/1023.0);
	//
	//	radio.write(&voltage0, sizeof(voltage0));
	//	delay(5);
	//	radio.write(&voltage1, sizeof(voltage1));
	//	delay(5);
	//	radio.write(&voltage2, sizeof(voltage2));
	//	delay(5);
}
