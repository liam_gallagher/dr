#include <Arduino.h>
#include <Wire.h>
#include "mcp4728.h"

mcp4728 dac = mcp4728(0); // instantiate mcp4728 object, Device ID = 0

void printStatus();

void setup() {

	Serial.begin(9600);
	dac.begin();  // initialize i2c interface
	dac.vdd(5000); // set VDD(mV) of MCP4728 for correct conversion between LSB and Vout


	dac.setVref(1,1,1,1); // set to use internal voltage reference (2.048V)

	dac.setGain(1,1,1,1); // set the gain of internal voltage reference ( 0 = gain x1, 1 = gain x2 )


	dac.analogWrite(1500,1500,1500,0); // write to input register of DAC four channel (channel 0-3) together. Value 0-4095

	int id = dac.getId(); // return devideID of object
	Serial.print("Device ID  = "); // serial print of value
	Serial.println(id, DEC); // serial print of value
	printStatus(); // Print all internal value and setting of input register and EEPROM.

}
void loop() {

}
void printStatus()
{
	Serial.println("NAME     Vref  Gain  PowerDown  Value");
	for (int channel=0; channel <= 3; channel++)
	{
		Serial.print("DAC");
		Serial.print(channel,DEC);
		Serial.print("   ");
		Serial.print("    ");
		Serial.print(dac.getVref(channel),BIN);
		Serial.print("     ");
		Serial.print(dac.getGain(channel),BIN);
		Serial.print("       ");
		Serial.print(dac.getPowerDown(channel),BIN);
		Serial.print("       ");
		Serial.println(dac.getValue(channel),DEC);

		Serial.print("EEPROM");
		Serial.print(channel,DEC);
		Serial.print("    ");
		Serial.print(dac.getVrefEp(channel),BIN);
		Serial.print("     ");
		Serial.print(dac.getGainEp(channel),BIN);
		Serial.print("       ");
		Serial.print(dac.getPowerDownEp(channel),BIN);
		Serial.print("       ");
		Serial.println(dac.getValueEp(channel),DEC);
	}
	Serial.println(" ");
}
